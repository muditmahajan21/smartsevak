const hospitalsRouter = require('express').Router()
const request = require('request');

const distance = (lat1, lon1, lat2, lon2) => {
    lon1 =  lon1 * Math.PI / 180;
    lon2 = lon2 * Math.PI / 180;
    lat1 = lat1 * Math.PI / 180;
    lat2 = lat2 * Math.PI / 180;

    let dlon = lon2 - lon1;
    let dlat = lat2 - lat1;
    let a = Math.pow(Math.sin(dlat / 2), 2)
                + Math.cos(lat1) * Math.cos(lat2)
                * Math.pow(Math.sin(dlon / 2),2);
            
    let c = 2 * Math.asin(Math.sqrt(a));

    let r = 6371;

    return(c * r);
}

const sortData = (hospitalData, lat, lng) => {
    let toSortData = JSON.parse(JSON.stringify(hospitalData));
    
    for(let i = 0; i < toSortData.data.length; i++) {
        toSortData.data[i].distance = distance(lat, lng, toSortData.data[i].lat, toSortData.data[i].lng)
    }

    let sortedData = toSortData.data.sort((a, b) => a.distance - b.distance)
    
    for(let i = 0; i < sortedData.length; i++) {
        delete sortedData[i]["lat"]
        delete sortedData[i]["lng"]
        sortedData[i].distance = sortedData[i].distance.toString() + " KM"
    }

    return sortedData
}

let hospitalData = []
request({
    method: 'GET',
    uri: 'https://apitest2.smartsevak.com/places',
    }, (error, response, body) => {
        if(error) {
            console.log(error)
            return
        } 
        hospitalData = response.body
        hospitalData = JSON.parse(hospitalData)
        if(response.statusCode == 200) {
             console.log('Successful external api call')
             console.log(hospitalData)
        }
        else {
            console.log('Error with api call')
        }
    }
)

hospitalsRouter.get('/', async (request, response) => {
    let lat = request.query.lat
    let lng = request.query.lng

    console.log(lat, lng)
    const sortedData = sortData(hospitalData, lat, lng)

    response.json(sortedData)
})

module.exports = hospitalsRouter