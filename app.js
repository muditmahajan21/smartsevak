const express = require('express')
const app = express()
const hospitalsRouter = require('./controllers/hospitals')

app.use('/hospitals', hospitalsRouter)

module.exports = app